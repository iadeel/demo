# Demo App

# Clone repo from bitbucket https://bitbucket.org/iadeel/demo

Run npm install to install Dependencies


#File structure
#-lib
    -helpFunc.js        -- Contain code email and Push notification
    -thumbnail.js       -- Function to create thumbnail of uploaded images
#-middleware 
    -authentication     -- Middleware for user Authorization
#-models
    -contain mongoose models
#-public 
    -Frontend section
#routers
    -Express Router 