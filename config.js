var conf = {
        "environment": "development",
        "server": {
                "port": 3000,
        },
        "db": {
                "dbName": "demoDB",
                "port": 27017,
                "host": "localhost"
        },
        "mail": {
                "name" : "gmail"
        }
};

module.exports = conf;
