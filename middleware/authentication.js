var jwt = require('jsonwebtoken'),
    md5 = require('md5');

module.exports = {
  authenticate: function(request, response, next) {
    var token = request.headers.token;
    try {
      var decoded = jwt.verify(token, 'demo_token');
      var email = md5(request.headers.email);
      if(email != decoded.publicKey){
        response.status(401).send({'err': 'authorized'}).end();
      }
      next()
    } catch(err) {
      response.status(401).send({'err': err.message}).end();
    }
  }
}
