var mongoose =  require('mongoose');
var Schema = mongoose.Schema;
var Q = require('q');
var md5 = require('md5');

var userSchema = new Schema({
  	username: { type: String, required: true },
	password: { type: String, required: true },
	dob: { type: String, default: '' },
	number: { type: String, required: true },
	cnic: { type: String, default: '' },
	picture: { type: String, default: '' },
	createdAt: { type: Date, default: Date.now }
});



userSchema.statics.login = function login(data) {
	data.password = md5(data.password);
	var defered = Q.defer();
		this.findOne(data,{"__v" : 0}, function (err,result){
		if(result == null){
			defered.resolve({"code" : 'IC','message':'No User Found. username or password is incorrect.'});
		}else{
			if(!err){
				result.save(function (error, userInfo){
					if(error){
						defered.reject(false);
					}else{
						defered.resolve(userInfo);
					}
				})
			}else{
				defered.reject(false);
			}
		}
	});

	return defered.promise;
};



var userModel = mongoose.model('UserModel', userSchema);
module.exports = userModel;
