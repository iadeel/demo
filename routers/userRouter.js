var express = require('express'),
	router = express.Router(),
	userModel = require('../models/userModel'),
	moment = require('moment'),
	helperFun = require('../lib/helperFunc'),
	md5 = require('md5'),
	jwt = require('jsonwebtoken'),
	auth = require('../middleware/authentication.js');



router.post('/login', function (req, res){
	var data = {
		username: req.body.username,
		password: req.body.password
	}

	if((data.username == null || "") && (data.password == null || "")){
		response.status(400).send({"message" : "Parameters are missing."}).end();
	}else{
		userModel.login(data)
		.then(function(result){
			if(result.code == 'IC'){
				res.status(404).send({"message" : result.message}).end();
			}else{
				return res.status(200).send({data : result}).end();
			}
		})
		.catch(function(err){
			res.status(500).send({"message" : "Server Error. Please try again later...", "err" : err});
		})

	}
});

router.post('/register', function (req, res){

	/**
		check if parameters are not available in Request Payload
	**/
	if((req.body.username == null || "") || (req.body.password == null || "") || (req.body.cnic == null || "")){
		return res.status(400).send({"message" : "Parameters are missing."}).end();
	}
	var encrytPass = md5(req.body.password);
	var data = {
		username: req.body.username,
		number: req.body.number,
		password: encrytPass,
		dob: req.body.dob,
		cnic: req.body.cnic
	}

	userModel.findOne({ $and:[ {'username':data.username}, {'number': data.number}]},function (err, user){
		if(err){
				return res.status(400).send({"message" : err}).end();
		}
		if(user){
				return res.status(200).send(user).end();
		}
		var newUser = new userModel(data);
		newUser.save(function (err,newUser){
			if(err){
				return res.status(400).send({"message": "Validation Error","err" : err}).end();
			}

				return res.status(200).send(newUser).end();
		})
	})

});


router.post('/update', function (req, res){
	var userId = req.headers.userid;
	var user = {
		username: req.body.username,
		number: req.body.number,
		dob: req.body.dob,
		cnic: req.body.cnic
	}

	userModel.findOne({"_id" : userId},function (err,User){
		if(err){
			return res.status(400).send({"message" : "Internal Server Error", "err" : err}).end();
		}
		if(User == null){
			return res.status(400).send({"message" : "Invalid User ID"}).end();
		}
		User.username = user.username;
		User.number = user.number;
		User.dob = user.dob;
		User.cnic = user.cnic;
		User.save(function (error,result){
			if(error){
				return res.status(500).send({"message" : "Internal Server Error", "err" : error}).end();
			}
			delete result.__v;
			res.status(200).send({'msg': 'Updated Successfully', 'data': result }).end();
		})
	})

});


module.exports = router;
